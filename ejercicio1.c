#include <stdio.h>

void main() {
    int asientos[5][5], opcion, asiento = 1, fil, col, comprar, verificar;
    double ganancias = 0.00;
    for (fil = 0; fil < 5; fil++) {
        for (col = 0; col < 5; col++) {
            asientos[fil][col] = asiento;
            asiento = asiento + 1;
        }
    }
    printf("Asientos disponibles\n");
    printf("---------------------------------------");
    for (fil = 0; fil < 5; fil++) {
        printf("\n");
        for (col = 0; col < 5; col++) {
            printf("%d\t", asientos[fil][col]);
        }
    }
    printf("\n------------------------------");
    printf("\n¿Qué desea realizar?\n1. Compra de entradas\n2. Mostrar las ganancias\nCualquier otra tecla para salir\n");
    scanf("%d", &opcion);
    printf("\n-----------------------------------\n");

    while (opcion > 0 && opcion < 3) {
        switch (opcion) {
            case 1:
                
                printf("P A N T A L L A\n");
                printf("-----------------------------------");
                //Con estas iteraciones mostramos la matriz
                for (fil = 0; fil < 5; fil++) {
                    printf("\n");
                    for (col = 0; col < 5; col++) {
                        printf("%d\t", asientos[fil][col]);
                    }
                }

                printf("\nIngrese el número del asiento\n");
                scanf("%d", &comprar);
                while (asiento < 0 || comprar > 25) {
                    printf("\nEl asiento no existe.\nIntente nuevamente ingrsando un asiento comprendido entre 1 y 25\n");
                    printf("\n\nIngrese el número del asiento\n");
                    scanf("%d", &comprar);
                }
                int cont;
                cont=0;
                for (fil = 0; fil < 5; fil++) {
                    for (col = 0; col < 5; col++) {
                        if (comprar == asientos[fil][col]) {
                            cont = 1;
                            asientos[fil][col] = 0;
                            if (fil == 0) {
                                printf("\nEl monto a pagar es de $5\n\n");
                                printf("---------------------");
                                ganancias = ganancias + 5.00;
                            } else if (fil == 4) {
                                printf("\nEl monto a pagar es de $2.50\n\n");
                                printf("---------------------");
                                ganancias = ganancias + 2.50;
                            } else {
                                printf("\nEl monto a pagar es de $3.50\n\n");
                                printf("---------------------");
                                ganancias = ganancias + 3.50;
                            }
                        }
                    }
                }
                if (cont < 1) {
                    printf("\nAsiento no valido debido a que ya lo compraron\n");
                }


                for (fil = 0; fil < 5; fil++) {
                    printf("\n");
                    for (col = 0; col < 5; col++) {
                        printf("%d\t", asientos[fil][col]);
                    }
                }
                printf("\n-----------------------------------");
                printf("\n¿Qué desea realizar?\n1. Compra de entradas\n2. Mostrar las ganancias\nCualquier otra tecla para salir\n");
                scanf("%d", &opcion);
                printf("\n-----------------------------------\n");
                break;
            case 2:
                printf("\nG A N A N C I A S\n");
                printf("$%.2f", ganancias);
                printf("\n-----------------------------------");
                printf("\n¿Qué desea realizar?\n1. Compra de entradas\n2. Mostrar las ganancias\nCualquier otra tecla para salir\n");
                scanf("%d", &opcion);
                printf("\n-----------------------------------\n");
            default:
                printf("Opcion no valida");



        }
    }
}


