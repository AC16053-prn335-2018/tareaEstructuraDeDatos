#include <stdio.h>

void main() {
    int tamanio, i, delPosicion;
    printf("Ingrese el tamaño del vector\n");
    scanf("%d", &tamanio);
    int vector[tamanio];
    for (i = 0; i < tamanio; i++) {
        printf("Ingrese un número en la posición %d del vector\n", i + 1);
        scanf("%d", &vector[i]);
    }
    printf("\n¿Qué posición desea eliminar?\n");
    scanf("%d", &delPosicion);
    for (i = delPosicion - 1; i <= tamanio; i++) {
        vector[i] = vector[i + 1];
    }
    tamanio = tamanio - 1;
    printf("El  vector con el dato eliminado es: \n");
    for (i = 0; i < tamanio; i++) {
        if (i < tamanio - 1) {
            printf("%d, ", vector[i]);
        } else {
            printf("%d", vector[i]);
        }
    }
}
